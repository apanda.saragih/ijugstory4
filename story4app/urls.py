from django.urls import path
from . import views

app_name = 'story4app'

urlpatterns = [
    path('', views.story_4, name='home'),
    path('characters/', views.characters, name='characters'),
    path('experiences/', views.experiences, name='experiences'),
]