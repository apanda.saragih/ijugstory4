from django.shortcuts import render

# Create your views here.
def story_4(request):
    return render(request, 'Story3.html')

def characters(request):
    return render(request, 'Characters.html')
def experiences(request):
    return render(request, 'Experiences.html')
